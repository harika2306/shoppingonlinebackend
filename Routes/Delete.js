const express=require('express')
const db= require('mongodb')

const mongoclient=require('mongodb').MongoClient;

 const router= express.Router();
 
 module.exports= router.get('/',(req,res)=>{
    const bname = (req.body.bname);
    const type= (req.body.type);
    const colour=(req.body.colour);
    const size=(req.body.size);
    const price=(req.body.price);  
 

    mongoclient.connect('mongodb://localhost:1919/test',(err,db)=>{
    if(err){
        res.status(500).send("internal server error");
        return;
    }
    else{
        console.log("db connected")
        db.collection('products').deleteOne({"bname":bname,
                                             "type":type,
                                             "colour":colour,
                                             "size":size,
                                             "price":price }, (err,data)=>{
            if(err) throw err;
            else{
                res.send("deleted successfully")
            }
        })
    }
    })
 })