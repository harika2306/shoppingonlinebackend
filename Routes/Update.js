const express=require('express');

const router= express.Router();

const db= require('mongodb');

const mongoclient=require('mongodb').MongoClient;

module.exports= router.put('/',(req,res)=>{
    
    mongoclient.connect('mongodb://localhost:1919/test',(err,db)=>{
    if(err){
        res.status(500).send("internal service error");
        return;
    }
    else{
        console.log("db connected")
        db.collection('products').updateOne({"bname":req.body.bname},{$set:{"type": req.body.type,
                                                                            "colour":req.body.colour,
                                                                            "size": req.body.size,
                                                                            "price": req.body.price,
                                                                            "password":req.body.password,
                                                                            }},(err,data)=>{
            if(err) throw err;
            else{
                res.status(201);
                res.send(data);
            }
        })
    }
    })
 })